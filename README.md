# Test

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Installation

```sh
git clone https://gitlab.com/hharrari/small-project
```

### Configuration des environnements

- Création des fichiers à la racine .env.local et ajouter votre configuration Base de données `DATABASE_URL`
- Exemple :
`APP_ENV=dev`
`DATABASE_URL=mysql://root:root@127.0.0.1:8889/small_project_dev?serverVersion=5.7`
- Modification dans le .env.test la variable `DATABASE_URL` en fonction de votre conf Mysql

### Installation des dépendences / de la BDD / migrations et des Fixtures de dev et de tests

```sh
composer install
composer generate
composer generate-test
```

### Lancer le serveur

Démarrer le serveur

```
php -S localhost:8000 -t public
```

OU si vous avez la  Symfony CLI d'installer

```
symfony serve
```

### Lancer des tests

```bash
APP_ENV=test php ./vendor/bin/phpunit --testdox
```

### Lancer des tests avec le mode coverage

```bash
APP_ENV=test php bin/phpunit --coverage-html var/log/test/test-coverage
```

Vous pouvez ensuite ouvrir sur un navigateur le fichier var/log/test/test-coverage/index.html

🚨 Vous devez installer l'extention xdebugg et activer le mode coverage dans php.ini

<https://xdebug.org/docs/install#pecl> 🛠

## Plugins

| Plugins et templates | README |
| ------ | ------ |
| lumen bootwatch | [Bootwatch.com][lumen] |
| Faker-PicsumPhotos | [plugins/bluemmb/README.md][picsum-faker] |
| Doctrine Fixtures Bundle | [plugins/doctrine/README.md][DoctrineFixturesBundle] |
| Symfony Tests Pack | [plugins/symfony/test-pack/README.md][sf-test-pack] |

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [lumen]: <https://bootswatch.com/lumen/>
   [picsum-faker]: <https://github.com/bluemmb/Faker-PicsumPhotos>
   [DoctrineFixturesBundle]: <https://github.com/doctrine/DoctrineFixturesBundle>
   [sf-test-pack]: <https://github.com/symfony/test-pack>
