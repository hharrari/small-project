<?php
namespace App\Tests\Entity\Housing;

use App\Entity\Housing;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HousingEntityTest extends KernelTestCase
{
   
    /**
     * check assert has error with validation service
     *
     * @param Housing $housing
     * @param integer $number
     * @return void
     */
    public function assertHasErrors(Housing $housing, int $number = 0): void
    {
        self::bootKernel();

        $errors = self::$container->get('validator')->validate($housing);

        $messages = [];
        /**
         * Display messages errors
         * @var ConstraintViolation $errors
         */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        
        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    /**
     * Data use for true test
     */
    const TRUE_ADDRESS = "Adresse test";
    const TRUE_FLOOR = 2;
    const TRUE_ELEVATOR = true;
    const TRUE_ROOM = 4;
    const TRUE_MAIN_PICTURE = "http://test-photo.fr";

    /**
     * Data use for false test
     */
    const FALSE_ID = 2;
    const FALSE_ADDRESS = "False adresse test";
    const FALSE_FLOOR = 22;
    const FALSE_ELEVATOR= false;
    const FALSE_ROOM = 44;
    const FALSE_MAIN_PICTURE = "http://false-test-photo.fr";

    /**
    * Data use for false test
    */
    const FAIL_ADDRESS = "a";
    const FAIL_FLOOR = -1;
    const FAIL_ROOM = -1;
    const FAIL_MAIN_PICTURE = "//fails-test-photo.fr";

    public function getEntity() : Housing
    {
        return (new Housing())
        ->setAddress(self::TRUE_ADDRESS)
        ->setFloor(self::TRUE_FLOOR)
        ->setElevator(self::TRUE_ELEVATOR)
        ->setRoom(self::TRUE_ROOM)
        ->setMainPicture(self::TRUE_MAIN_PICTURE);
    }

    public function testIsTrue(): void
    {
        $housing = $this->getEntity();

        $this->assertTrue($housing->getId() === null);
        $this->assertTrue($housing->getAddress() === self::TRUE_ADDRESS);
        $this->assertTrue($housing->getFloor() === self::TRUE_FLOOR);
        $this->assertTrue($housing->getElevator() === self::TRUE_ELEVATOR);
        $this->assertTrue($housing->getRoom() === self::TRUE_ROOM);
        $this->assertTrue($housing->getMainPicture() === self::TRUE_MAIN_PICTURE);
        $this->assertTrue($housing->getCreatedAt() instanceof \DateTimeImmutable);
        $this->assertTrue($housing->getUpdateAt() === null);
    }

    public function testIsFalse(): void
    {
        $housing = $this->getEntity();

        $this->assertFalse($housing->getId() === self::FALSE_ID);
        $this->assertFalse($housing->getAddress() === self::FALSE_ADDRESS);
        $this->assertFalse($housing->getFloor() === self::FALSE_FLOOR);
        $this->assertFalse($housing->getElevator() === self::FALSE_ELEVATOR);
        $this->assertFalse($housing->getRoom() === self::FALSE_ROOM);
        $this->assertFalse($housing->getMainPicture() === self::FALSE_MAIN_PICTURE);
        $this->assertFalse($housing->getCreatedAt() === null);
        $this->assertFalse($housing->getUpdateAt() !== null);
    }

    public function testIsEmpty(): void
    {
        $housing = new Housing();

        $this->assertEmpty($housing->getAddress());
        $this->assertEmpty($housing->getFloor());
        $this->assertEmpty($housing->getElevator());
        $this->assertEmpty($housing->getRoom());
        $this->assertEmpty($housing->getMainPicture());
        $this->assertEmpty($housing->getUpdateAt());
    }

    public function testIsNotEmpty(): void
    {
        $housing = new Housing();
        $this->assertTrue($housing->getCreatedAt() instanceof \DateTimeImmutable);
    }

    public function testValidEntity() : void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testInvalidAddressEntity(): void
    {
        $this->assertHasErrors($this->getEntity()->setAddress(''), 2);
        $this->assertHasErrors($this->getEntity()->setAddress(self::FAIL_ADDRESS), 1);
    }

    public function testInvalidFloorEntity(): void
    {
        $this->assertHasErrors($this->getEntity()->setFloor(self::FAIL_FLOOR), 1);
    }

    public function testInvalidRoomEntity() : void
    {
        $this->assertHasErrors($this->getEntity()->setRoom(self::FAIL_ROOM), 1);
    }

    public function testInvalidMainPictureEntity() : void
    {
        $this->assertHasErrors($this->getEntity()->setMainPicture(self::FAIL_MAIN_PICTURE), 1);
    }
}
