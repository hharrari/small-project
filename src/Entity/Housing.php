<?php

namespace App\Entity;

use App\Repository\Housing\HousingRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=HousingRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("address", message="Cette adresse est déjà prise, merci de la changer. !")
 */
class Housing
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(
     *     message = "Le champs adresse est obligatoire."
     * )
     * @Assert\Length(min=10, max=255, minMessage="l'adresse doit comporter au moins 10 charactères" )
     */
    private ?string $address = null;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(
     * message = "Le champs étage est obligatoire."
     * )
     * @Assert\PositiveOrZero(
     *    message = "La valeur de l'étage ne peut pas être négative."
     * )
     */
    private ?int $floor = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $elevator = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $updatedAt = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Url(
     *    message = "L'url '{{ value }}' n'est pas une url valide",
     * )
     */
    private ?string $mainPicture = null;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(
     *     message = "Le champs nombre de pièces est obligatoire."
     * )
     * @Assert\PositiveOrZero(
     *    message = "La valeur ne peut pas être négative."
     * )
     */
    private ?int $room = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getFloor(): ?int
    {
        return $this->floor;
    }

    public function setFloor(?int $floor): self
    {
        $this->floor = $floor;

        return $this;
    }

    public function getElevator(): ?bool
    {
        return $this->elevator;
    }

    public function setElevator(bool $elevator): self
    {
        $this->elevator = $elevator;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setupdatedAtValue(): self
    {
        $this->updatedAt = new \DateTimeImmutable();

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getMainPicture(): ?string
    {
        return $this->mainPicture;
    }

    public function setMainPicture(?string $mainPicture): self
    {
        $this->mainPicture = $mainPicture;

        return $this;
    }

    public function getRoom(): ?int
    {
        return $this->room;
    }

    public function setRoom(?int $room): self
    {
        $this->room = $room;

        return $this;
    }
}
