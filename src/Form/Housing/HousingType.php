<?php

namespace App\Form\Housing;

use App\Entity\Housing;
use App\Form\FormTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class HousingType extends AbstractType
{
    use FormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('address', TextType::class, $this->setOptions("Indiquer l'adresse du bien"))
            ->add('floor', IntegerType::class, $this->setOptions("Renseigner l'étage", [
                'attr' => ["min" => 0]
            ]))
            ->add('room', IntegerType::class, $this->setOptions("Renseigner le nombre de pièces", [
                'attr' => ["min" => 0]
            ]))
            ->add('elevator', ChoiceType::class, $this->setOptions("Présence d'un ascenceur ? ", [
                "choices" => [
                    "Oui" => true,
                    "Non" => false
                ]
            ]))
            ->add('mainPicture', UrlType::class, $this->setOptions("Veuillez rentrer une url d'image", [
                "required" => false
            ]))
            ->add('submit', SubmitType::class, $this->setOptions('Sauvegarder'));
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Housing::class,
        ]);
    }
}
