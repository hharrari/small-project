<?php

namespace App\Controller\Housing;

use App\Entity\Housing;
use App\Form\Housing\HousingType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Subscriber\Housing\Events\HousingEditEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Update extends AbstractController
{

    /**
     * @var EventDispatcherInterface $dispatcher
     */
    protected $dispatcher;

    /**
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Edition Housing
     * @Route("/{id}/edition", name="housing_edition", requirements={"id": "\d+"})
     *
     * @param Housing $housing
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
    */
    public function edit(
        Housing $housing,
        Request $request,
        EntityManagerInterface $em
    ): Response {
        $form = $this->createForm(HousingType::class, $housing)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash("success", "Votre appartement à bien été mis à jour");

            // Create event edit housing
            $housingEvent = new HousingEditEvent($housing);
            $this->dispatcher->dispatch($housingEvent, HousingEditEvent::NAME);

            return $this->redirectToRoute(
                "housing_show",
                [
                'id' => $housing->getId()
                ]
            );
        }
        return $this->render("/housing/edit.html.twig", [
            'form' => $form->createView()
        ]);
    }
}
