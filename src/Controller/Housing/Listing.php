<?php

namespace App\Controller\Housing;

use App\Entity\Housing;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Listing extends AbstractController
{

    /**
     * Show Housing details
     * @Route("/{id}/show", name="housing_show", requirements={"id": "\d+"}, methods="GET")
     *
     * @param Housing $housing
     * @return Response
     */
    public function show(Housing $housing): Response
    {
        return $this->render(
            "/housing/show.html.twig",
            [
            'housing' => $housing
            ]
        );
    }
}
