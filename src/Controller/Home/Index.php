<?php

namespace App\Controller\Home;

use App\Repository\Housing\HousingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Index extends AbstractController
{
    /**
     * HomePage list Housing by created At
     * @Route("/", name="home_index")
     *
     * @param HousingRepository $housingRepository
     * @return Response
     */
    public function index(HousingRepository $housingRepository): Response
    {
        $housings = $housingRepository->findBy([], ['createdAt' => 'DESC']);
        return $this->render('home/index.html.twig', [
            'housings' => $housings,
        ]);
    }
}
