<?php

namespace App\DataFixtures;

use App\Entity\Housing;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

/**
 * @codeCoverageIgnore
 */

class AppFixtures extends Fixture
{
    /**
     * Fake Fixtures
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));
        $faker->addProvider(new \Bluemmb\Faker\PicsumPhotosProvider($faker));

        for ($h = 0; $h < 15; $h++) {
            $housing = new Housing();

            $housing->setAddress($faker->address())
            ->setElevator($faker->boolean())
            ->setFloor(mt_rand(1, 5))
            ->setMainPicture($faker->imageUrl(800, 800, true))
            ->setRoom(mt_rand(1, 5));

            $manager->persist($housing);
        }

        $manager->flush();
    }
}
