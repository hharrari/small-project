<?php

namespace App\Subscriber\Housing\Dispatcher;

use Psr\Log\LoggerInterface;
use App\Subscriber\Housing\Events\HousingEditEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class HousingEditSubscriber implements EventSubscriberInterface
{
    /**
     * @var LoggerInterface $logger
     */
    protected $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public static function getSubscribedEvents()
    {
        return [HousingEditEvent::NAME => 'notifyAdmin'];
    }

    /**
     * Notification by event after edit housing
     *
     * @param HousingEditEvent $housingEditEvent
     * @return void
     */
    public function notifyAdmin(HousingEditEvent $housingEditEvent)
    {
        //Todo Example: Notify admin for checking update housing
        $this->logger->info(
            "L'appartement n° : '
            " . $housingEditEvent->getHousing()->getId() . "
            ' vient d'être modifié "
        );
    }
}
