<?php

namespace App\Subscriber\Housing\Events;

use App\Entity\Housing;
use Symfony\Contracts\EventDispatcher\Event;

class HousingEditEvent extends Event
{
    public const NAME = 'housing.edit';

    /**
     *
     * @var Housing $housing
     */
    protected $housing;

    /**
     * Event Housing construct
     *
     * @param Housing $housing
     */
    public function __construct(Housing $housing)
    {
        $this->housing = $housing;
    }

    /**
     * Get Housing Entity
     *
     * @return Housing
     */
    public function getHousing(): Housing
    {
        return $this->housing;
    }
}
